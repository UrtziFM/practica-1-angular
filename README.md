## Angular Boilerplate
Changed
Clone this project and run `npm install` to install all dependencies.

After that, run `npm start` to start the development server.

### Build

To build the project, run `npm run build` and it will produce a `/dist` folder that will contain your project.

That output can be uploaded to your storage system (aka server or instance) and will run the `index.html` file, making your project work live.

In case you need to test your production ready app before deploying, run `npm run serve` and a local server will start with the build files inside the `/dist` folder.