import { Component, OnInit } from '@angular/core';
import { IFood } from '../interfaces/food.interface';
import { apiResponse } from '../api/foods.api';

@Component({
  selector: 'app-foods',
  templateUrl: './foods.component.html',
  styleUrls: ['./foods.component.scss']
})
export class FoodsComponent implements OnInit {

  constructor() { }
  public foods : IFood[];
  ngOnInit() {
     this.foods = apiResponse;
  }

}
