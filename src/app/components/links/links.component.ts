import { Component, OnInit } from '@angular/core';
import { ILinks } from '../interfaces/links.inteface';
import { apiResponse } from '../api/links.api';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {

  constructor() { }

  public links : ILinks []

  ngOnInit() {

    this.links = apiResponse;
  }

}
